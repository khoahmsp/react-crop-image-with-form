import React, { useCallback, useEffect, useRef, useState } from 'react';
import ReactCrop from 'react-cropper';
import { useWatch } from 'react-hook-form';

export default function CropImage2({ control }) {
  const image = useWatch({
    control,
    name: 'image',
  });

  const [imageSrc, setImageSrc] = useState('');

  useEffect(() => {
    if (image?.[0]) {
      const fr = new FileReader();
      fr.onload = () => {
        setImageSrc(fr.result);
      };
      fr.readAsDataURL(image[0]);
    }
  }, [image]);

  const imgRef = useRef(null);
  const previewCanvasRef = useRef(null);
  const [crop, setCrop] = useState({ aspect: 16 / 9 });
  console.log('🚀 ~ file: CropImage2.jsx ~ line 26 ~ CropImage2 ~ crop', crop);
  const [completedCrop, setCompletedCrop] = useState(null);
  console.log(
    '🚀 ~ file: CropImage2.jsx ~ line 27 ~ CropImage2 ~ completedCrop',
    completedCrop
  );

  const onLoad = useCallback((img) => {
    imgRef.current = img;
  }, []);

  return (
    <div>
      <ReactCrop
        src={imageSrc}
        // onImageLoaded={onLoad}
        crop={crop}
        onChange={(c) => {
          console.log('here');
          setCrop(c);
        }}
        onComplete={(c) => setCompletedCrop(c)}
      />
      <div>
        <canvas
          ref={previewCanvasRef}
          // Rounding is important so the canvas width and height matches/is a multiple for sharpness.
          style={{
            width: Math.round(completedCrop?.width ?? 0) / 10,
            height: Math.round(completedCrop?.height ?? 0) / 10,
          }}
        />
      </div>
    </div>
  );
}
