import React, { useEffect, useRef, useState } from 'react';
import Cropper from 'react-cropper';
import 'cropperjs/dist/cropper.css';
import { useWatch } from 'react-hook-form';

const CropImage = ({ control }) => {
  const [imageSrc, setImageSrc] = useState('');
  const [previewImage, setPreviewImage] = useState(null);

  const image = useWatch({
    control,
    name: 'image',
  });
  useEffect(() => {
    if (image?.[0]) {
      const fr = new FileReader();
      fr.onload = () => {
        setImageSrc(fr.result);
      };
      fr.readAsDataURL(image[0]);
    }
  }, [image]);

  const cropperRef = useRef(null);
  const onCrop = () => {
    const imageElement = cropperRef?.current;
    const cropper = imageElement?.cropper;
    // console.log(cropper.getCroppedCanvas().toDataURL());
    setPreviewImage(cropper.getCroppedCanvas().toDataURL());
  };

  return (
    <>
      <Cropper
        src={imageSrc}
        style={{ width: 200, height: 'auto' }}
        // Cropper.js options
        aspectRatio={16 / 9}
        guides={false}
        ref={cropperRef}
        cropend={onCrop}
      />
      {previewImage && <img src={previewImage} alt="preview" />}
    </>
  );
};

export default CropImage;
