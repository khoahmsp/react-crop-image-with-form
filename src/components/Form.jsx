import React, { useEffect } from 'react';
import { useForm } from 'react-hook-form';
import CropImage from './CropImage.jsx';
import CropImage2 from './CropImage2.jsx';
import UploadImage from './UploadImage.jsx';

export default function Form() {
  //#region useForm
  const { watch, register, control, getValues } = useForm();
  const watchSize = watch('size', '1x1');
  const watchImage = watch('image');
  //#endregion

  //#region helpers
  const parseSizeToScaleFactor = (strSize = '1x1') => {
    const arr = strSize.split('x').map((str) => parseInt(str));
    const scaleFactor = {
      x: arr[0],
      y: arr[1],
    };
    return scaleFactor;
  };
  //#endregion

  //#region useEffect
  useEffect(() => {
    if (watchImage instanceof FileList && watchImage.length > 0) {
      const reader = new FileReader();

      const file = watchImage[0];
      const name = 'image' + Date.now();

      const sources = [
        {
          name,
          type: 'texture',
          path: '',
        },
      ];

      reader.onload = function (e) {
        sources[0].path = e.target.result;
      };
      reader.readAsDataURL(file);
    }
  }, [watchImage, getValues]);

  useEffect(() => {
    console.log(parseSizeToScaleFactor(watchSize));
  }, [watchSize]);
  //#endregion

  return (
    <form>
      {/* Select size */}
      <select id="sizeSelect" {...register('size')}>
        <option value="1x1">1x1</option>
        <option value="1x2">1x2</option>
        <option value="2x3">2x3</option>
        <option value="3x5">3x5</option>
      </select>
      {/* Upload image */}
      <label htmlFor="upload-image-input">
        {/* <UploadImage control={control} outerWidth="200px" innerWidth="175px" /> */}
        Upload
      </label>
      <input
        id="upload-image-input"
        type="file"
        accept="image/*"
        {...register('image')}
        style={{ display: 'none' }}
      />
      <CropImage control={control} />
      <CropImage2 control={control} />
    </form>
  );
}
